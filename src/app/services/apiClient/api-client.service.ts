import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiClientService {

  /** assign our api domain + prefix */
  apiDomain = 'https://60439ecaa20ace001728e4ad.mockapi.io/api/';

  /** just basic header */
  httpHeader = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient
  ) { }

  /** basically just http get that use /users/:id api */
  verifyUser(id) {

    const url = this.apiDomain + 'users/' + id;

    return this.http.get(url);
  }

  //** u can more method for api here for global use */

}
