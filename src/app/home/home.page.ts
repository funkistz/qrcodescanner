import { Component } from '@angular/core';
import { AlertController, ToastController, Platform, NavController, LoadingController } from '@ionic/angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { ApiClientService } from '../services/apiClient/api-client.service';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  verifyApi = 'https://60439ecaa20ace001728e4ad.mockapi.io/api/';
  isAlert = true;
  isPage;
  qrcodeInput;

  /** this is global loading. we use this to control if loading will be shown or hidden */
  loading;

  /** this is global var for user. after we retrieve user from url we will assign it here to show on the page */
  user;

  constructor(
    private alertController: AlertController,
    private toastController: ToastController,
    private qrScanner: QRScanner,
    private apiClientService: ApiClientService,
    public loadingController: LoadingController,
    public navCtrl: NavController
  ) { }

  /** event will return true or false. This is default value for (ngModelChange) */
  changeToAlert(event) {
    this.isPage = !event;
    this.isAlert = event;
  }

  /** event will return true or false. This is default value for (ngModelChange) */
  changeToPage(event) {
    this.isAlert = !event;
    this.isPage = event;
  }

  /** Real qr code scanner function */
  scanQRCode() {

    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted


          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);

            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning

            /** we will process the qrcode after successfully read */
            if (this.isAlert) {
              this.QRCodeProcessorAlert(text);
            } else if (this.isPage) {
              this.QRCodeProcessorPage(text);
            }
          });

        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => {
        this.presentToast(e);
      });

  }

  /** Dummy qr code scanner to test using api */
  dummyScanQRCode(id) {

    if (this.isAlert) {
      this.QRCodeProcessorAlert(id);
    } else if (this.isPage) {
      this.QRCodeProcessorPage(id);
    }

  }

  /** qr code scanner processor for alert. success will display on the same page while error will popup alert. */
  QRCodeProcessorAlert(id) {

    /** show loading indicator */
    this.presentLoading();

    /** this is service that we create ourselve to process url. check the file ../services/apiClient  */
    this.apiClientService.verifyUser(id).subscribe(success => {
      console.log(success);

      /** dismiss loading indicator */
      this.dismissLoading();

      /** after user successfully retrieved we will assign it to global parameter. It then will show automatically in this page  */
      this.user = success;

    }, err => {
      console.log(err);

      /** dismiss loading indicator */
      this.dismissLoading();

      /** You can punt any custom message. This example use error message that retrieved from api. You can check console log for full response */
      this.presentAlert(err.message);
    });

  }

  /** qr code scanner processor for other page */
  QRCodeProcessorPage(id) {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: id
      }
    };
    this.navCtrl.navigateForward(['verify'], navigationExtras);

  }

  /** this is function is just for reassign user to null */
  backToScan() {
    this.user = null;
  }

  /** This is just function to display toast message */
  async presentToast(message, duration = 2000) {
    const toast = await this.toastController.create({
      message: message,
      position: 'top',
      duration: duration
    });
    toast.present();
  }

  /** This is just function to display alert */
  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Alert!',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  //** This is to present loading that appear forever untill dismissLoading() is called */
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await this.loading.present();
  }

  //** This is dismiss the loading */
  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
