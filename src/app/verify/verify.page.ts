import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController, Platform, NavController, LoadingController } from '@ionic/angular';
import { ApiClientService } from '../services/apiClient/api-client.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-verify',
  templateUrl: './verify.page.html',
  styleUrls: ['./verify.page.scss'],
})
export class VerifyPage implements OnInit {

  id;
  loading;
  user;
  error;

  constructor(
    private alertController: AlertController,
    private toastController: ToastController,
    private apiClientService: ApiClientService,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    /** we will retrieve parameter send here. in this case we will retrieve id */
    this.route.queryParams.subscribe(params => {

      /** assign id to global parameter */
      this.id = params.id;

      if (this.id) {
        this.QRCodeProcessor(this.id);
      }
    });
  }

  /** qr code scanner processor. */
  QRCodeProcessor(id) {

    /** show loading indicator */
    this.presentLoading();

    this.apiClientService.verifyUser(id).subscribe(success => {
      console.log(success);

      /** dismiss loading indicator */
      this.dismissLoading();

      /** after user successfully retrieved we will assign it to global parameter. It then will show automatically in this page  */
      this.user = success;

    }, err => {
      console.log(err);
      /** dismiss loading indicator */
      this.dismissLoading();

      /** You can punt any custom message. This example use error message that retrieved from api. You can check console log for full response */
      this.error = err.message;
    });

  }

  backToHome() {
    this.navCtrl.navigateBack(['home']);
  }


  /** This is just function to display toast message */
  async presentToast(message, duration = 2000) {
    const toast = await this.toastController.create({
      message: message,
      position: 'top',
      duration: duration
    });
    toast.present();
  }

  /** This is just function to display alert */
  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Alert!',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  //** This is to present loading that appear forever untill dismissLoading() is called */
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await this.loading.present();
  }

  //** This is dismiss the loading */
  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
